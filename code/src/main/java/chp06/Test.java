package chp06;

public class Test {
    public static void resize(Rectangle r) {
        while (r.getLength() <= r.getWidth()) {
            r.setLength(r.getLength() + 1);
        }
        System.out.println("It��s OK.");
    }

    public static void main(String args[]) {
        Rectangle r1 = new Rectangle();
        r1.setLength(5);
        r1.setWidth(15);
        resize(r1);

        Rectangle r2 = new Square();
        r2.setLength(5);
        r2.setWidth(15);
        resize(r2);
    }

}

