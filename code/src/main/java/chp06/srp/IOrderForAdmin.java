package chp06.srp;

public interface IOrderForAdmin extends IOrderForGet, IOrderForInsert {
    void modifyOrder();

    void deleteOrder();
}

