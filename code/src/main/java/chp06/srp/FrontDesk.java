package chp06.srp;

public class FrontDesk {

    public static void dealOrder(IOrderForGet order) {
        order.getOrder();
        // order.insertOrder();
    }

    public static void main(String args[]) {
        Order order = new Order();
        FrontDesk.dealOrder(new Order());
    }
}
