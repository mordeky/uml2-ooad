package chp06.srp;

public class Order implements IOrderForGet, IOrderForInsert, IOrderForAdmin {

    @Override
    public void modifyOrder() {
        System.out.println("modifyOrder");
    }

    @Override
    public void deleteOrder() {
        System.out.println("deleteOrder");
    }

    @Override
    public String getOrder() {
        System.out.println("dealOrder");
        return null;
    }

    @Override
    public void insertOrder() {
        System.out.println("insertOrder");
    }
}
