package chp06.srp;

public class FrontWebsite {

    public static void dealOrder(IOrderForInsert order) {
        // order.getOrder();
        order.insertOrder();
    }

    public static void main(String args[]) {
        Order order = new Order();
        FrontWebsite.dealOrder(new Order());
    }
}
