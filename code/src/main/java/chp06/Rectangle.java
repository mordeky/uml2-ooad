package chp06;

public class Rectangle {
    private int length;
    private int width;

    public void setLength(int l) {
        length = l;
    }

    public int getLength() {
        return length;
    }

    public void setWidth(int w) {
        width = w;
    }

    public int getWidth() {
        return width;
    }
}
