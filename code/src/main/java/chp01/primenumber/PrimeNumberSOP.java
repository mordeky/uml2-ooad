/**
 * @author Mordecai
 * @version 1.0.0
 * @create 2020/2/25 16:26
 * @blog
 */
package chp01.primenumber;

public class PrimeNumberSOP {
    public static void main(String args[]) {
        int n = 50, global_cnt = 0;
        int sieve[] = new int[n - 1];
        int iCounter = 2, iMax, i;
        for (i = 0; i < n - 1; i++) {
            sieve[i] = i + 2;
        }

        iMax = (int) Math.sqrt(n);
        while (iCounter <= iMax) {
            // 搜索所有能被 iCounter 整除的数【也就是搜索：2*iCounter, 3*iCounter, 4*iCounter】
            //   1) 而 iCounter 本身无须检验，因为 iCounter 已经经过了 2 ~ iCounter-1 的检验，
            //      因此，只需从 2*iCounter 开始检索即可；
            //   2) iCounter 所对应的索引位置在 iCounter - 2 处，
            //      因此，2*iCounter的索引位置在：2*iCounter - 2（也就是：(iCounter - 2) + iCounter）；
            //   3) 由上面的分析可知，更高效的索引位置应该从：(iCounter-1)*iCounter - 2 开始
            // ==================================（下面的解释不对）
            //    只要从 k = 2 * iCounter 的地方开始搜索即可
            //    因为 iCounter 要么为素数（不需排除），
            //    [要么为非素数，已经在 iCounter = 2 时被排除了]此处解释的不对！！
            // ==================================
            for (i = (iCounter - 1) * iCounter - 2; i < n - 1; i += iCounter) {
                global_cnt++;
                sieve[i] = 0;
            }
            // iCounter++;
            // while(sieve[iCounter-2] == 0) iCounter++;
            //do iCounter++; while(sieve[iCounter-2] == 0);
            while (sieve[++iCounter - 2] == 0) ;
            // for(;sieve[++iCounter - 2] != 0;);
        }

        for (i = 0; i < n - 1; i++)
            if (sieve[i] != 0) System.out.print(sieve[i] + " ");

        System.out.println("\n共迭代了：" + global_cnt + "次\n");
    }
}

