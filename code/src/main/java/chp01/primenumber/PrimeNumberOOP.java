package chp01.primenumber;

/**
 * @author Mordecai
 * @version 1.0.0
 * @create 2020/2/25 17:53
 * @blog
 */

class Item {
    public Item source;

    Item(Item src) {
        source = src;
        System.out.println("chp01.Item() is called");
    }

    int out() {
        return 0;
    }
}

class Counter extends Item {
    public int value;

    public int out() {
        return value++;
    }

    Counter(int v) {
        super(null);
        value = v;
    }
}

class Filter extends Item {
    public int factor;

    public int out() {
        while (true) {
            int n = source.out();
            if (n % factor != 0) return n;
        }
    }

    Filter(Item src, int f) {
        super(src);
        factor = f;
    }
}

// Sieve 负责维护整个Filter链表：
//     错误的理解：counter(2)->filter(factor=2)->filter(factor=3)->filter(factor=5)
//     准确地说，链表是这样的：counter(2)<-filter(factor=2)<-filter(factor=3)<-filter(factor=5)
//          也就是：由当前filter指向其source filter的，而非反过来

// Filter 负责：
//    1. 存放素数（即factor）；
//    2. 针对其source（也就是上一个Filter）的输出，判断其能否被factor整除

// 面向对象方法（OOP）与面向过程方法（SOP）在算法上不同：
//    SOP方法是：对于任一给定的数m，遍历所有的<=n的整数，排除所有的能被m整除的数
//    OOP方法是：对于任一给定的数m，遍历所有的比m小的素数（存放在Filter中），以判断当前数是否是素数

class Sieve extends Item {
    public int out() {
        int n = source.out();
        source = new Filter(source, n);
        return n;
    }

    Sieve(Item src) {
        super(src);
        System.out.println("chp01.Sieve() is called");
    }
}


public class PrimeNumberOOP {
    public static void main(String args[]) {
        Counter c = new Counter(2);
        Sieve s = new Sieve(c);
        int next, n = 50;

        while (true) {
            next = s.out();
            if (next > n) break;
            System.out.print(next + " ");
        }

        System.out.println();
    }
}
