package chp01.YusufRing;

/**
 * 题目要求：有n个人围成一圈，顺序排号。从第一个人开始报数（从1到M报数），
 * 凡报到 M 的人退出圈子，问最后留下的是原来第几号的那位。
 */

class CiNode {
    private Object data;
    public CiNode next;

    public CiNode(Object data, CiNode next) {
        super();
        this.data = data;
        this.next = next;
    }
}

public class CircularList {

    public static void main(String args[]) {
        // int n = 50, M = 3;
        // Item item = new Item(1, null);
        // Item head = item;
        // item.next = new Item(2, null);
        // item = item.next;
        // item.next = new Item(3, null);
        // item = item.next;
        // item.next = head;
        CiNode node4 = new CiNode("ddd", null);
        CiNode node3 = new CiNode("ccc", node4);
        CiNode node2 = new CiNode("bbb", node3);
        CiNode node1 = new CiNode("aaa", node2);
        CiNode first = new CiNode(null, node1);
        node4.next = first;
        return;
    }
}


