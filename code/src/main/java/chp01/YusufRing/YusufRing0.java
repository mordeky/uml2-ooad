package chp01.YusufRing;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

public class YusufRing0 {
    private static Scanner scanner = null;

    static {
        scanner = new Scanner(System.in);
    }

    public static void main(String[] args) {
        // System.out.println("请输入一共有多少人：");
        // int max_val = scanner.nextInt();
        // System.out.println("请输入要报的数：");
        // int M = scanner.nextInt();

        int n = 100, M = 5;
        Long atime = System.currentTimeMillis();
        System.out.println(new YusufRing0().getNum(n, M));
        Long btime = System.currentTimeMillis();
        Long ctime = btime - atime;
        System.out.println("使用的时间为：" + ctime + "毫秒.");
    }

    /**
     * 题目要求：有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），
     * 凡报到3的人退出圈子，问最后留下的是原来第几号的那位。
     *
     * @param n
     * @return
     */
    public String getNum(int n, int M) {
        if ((M <= 1) || (M >= n)) {
            System.out.println("ERROR!");
            System.exit(-1);
        }

        LinkedList<Map<String, Integer>> linkedList =
                new LinkedList<>();

        // 初始化将数据保存在队列中
        for (int i = 1; i <= n; i++) {
            Map<String, Integer> map = new HashMap<>();
            int value = i % M;
            map.put(String.valueOf(i), value);
            // if (value == 0)
            //     continue;
            linkedList.add(map);
        }

        int size = linkedList.size();

        while (size >= M) {
            // 挑选出报数为 M 的数据
            for (int i = 0; i < size; i++) {
                Map<String, Integer> map = linkedList.poll();
                String key = (String) map.keySet().toArray()[0];
                if (map.get(key) != 0) {
                    linkedList.add(map);
                }
            }

            size = linkedList.size();

            // 将队列中的元素重新赋值排序
            Map<String, Integer> lastMap = linkedList.getLast();
            String lastKey = (String) lastMap.keySet().toArray()[0];
            int lastValue = lastMap.get(lastKey);
            int firstValue = (lastValue + 1) % M;
            for (int i = 0; i < size; i++) {
                // if (firstValue != 0) {
                Map<String, Integer> map = linkedList.poll();
                String key = (String) map.keySet().toArray()[0];
                map.put(key, firstValue);
                linkedList.add(map);
                // }
                firstValue = (++firstValue) % M;
            }

        }

        // String resultKey = (String) linkedList.peek().keySet().toArray()[0];

        size = linkedList.size();
        String x = "";
        for (int i = 0; i < size; i++) {
            Map<String, Integer> map = linkedList.poll();
            String key = (String) map.keySet().toArray()[0];
            if (i < size-1)
                x += key + ", ";
            else
                x += key;
        }

        return x;
    }
}
