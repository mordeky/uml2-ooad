package chp01.YusufRing;

/**
 * 题目要求：有n个人围成一圈，顺序排号。从第一个人开始报数（从1到M报数），
 * 凡报到 M 的人退出圈子，问最后留下的是原来第几号的那位。
 */

class Item {
    public int value;
    public Item next;

    Item(int n, Item next) {
        value = n;
        this.next = next;
    }

    int out() {
        return 0;
    }
}

class Counter {
    private int value;
    public int n;
    private int M;

    public int out() {
        if (++value == M) {
            this.n--;
            value = 1;
            return 0;
        }
        return value;
    }

    Counter(int v, int n, int M) {
        value = v;
        this.n = n;
        this.M = M;
    }
}

class Circle {
    public int max_val;
    public boolean new_link;
    public Item head, end;
    public Counter counter;

    public int out() {
        int next_cnt = counter.out();
        boolean skip = next_cnt == 0;

        if (!new_link) {
            if(skip) {
                if (end.next == null) {
                    head = head.next;
                    end = head;
                    return 1;
                }
                end.next = end.next.next;
            }
        } else {
            int next_val = end.value + 1;
            if(skip) next_val++;
            if (next_val <= max_val)
                end.next = new Item(next_val, null);
            else {
                new_link = false;
                // if(skip)
                //     head = head.next;
                end.next = null;
                end = head;
                // end.next = head;
                return 1;
            }
        }

        end = end.next;
        if(end == null)
            end = head;

        return 0;
    }

    Circle(Item item, Counter counter, int max_val) {
        this.max_val = max_val;
        new_link = true;
        this.head = item;
        this.end = item;
        this.counter = counter;
    }
}

public class YusufRingOOP {

    public static void main(String args[]) {
        int n = 100, M = 5;
        Item item = new Item(1, null);
        Counter c = new Counter(1, n, M);
        Circle circle = new Circle(item, c, n);

        while (c.n >= M) circle.out();

        System.out.println();
    }
}


