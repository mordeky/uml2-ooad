package chp01.YusufRing;

import java.util.Scanner;

/**
 * 题目要求：有n个人围成一圈，顺序排号。从第一个人开始报数（从1到M报数），
 * 凡报到 M 的人退出圈子，问最后留下的是原来第几号的那位。
 */

public class YusufRing {
    private static Scanner scanner = null;

    static {
        scanner = new Scanner(System.in);
    }

    public static void main(String[] args) {
        // System.out.print("请输入一共有多少人：");
        // int max_val = scanner.nextInt();
        // System.out.print("请输入要报的数：");
        // int M = scanner.nextInt();
        // int n = 100, M = 3;
        int n = 100, M = 5;
        String x = YusufRing.getLeftMembers(n, M);

    }


    public static String getLeftMembers(int n, int M) {
        if ((M <= 1) || (M >= n)) {
            System.err.println("ERROR!");
            System.exit(-1);
        }

        int[] out_marker = new int[n];
        int left_num = n, out_num = 0, counter = 0;
        while (left_num >= M) {
            for (int i = 0; i < n; i++) {
                if (out_marker[i] == 1 || ++counter < M)
                    continue;
                out_num++;
                counter = 0;
                out_marker[i] = 1;
            }
            left_num = n - out_num;
        }

        counter = 0;
        String LeftMembers = "";
        for (int i = 0; i < n; i++) {
            if (out_marker[i] == 1)
                continue;
            counter++;

            LeftMembers += (i + 1);
            if (counter < M - 1)
                LeftMembers += ", ";
        }

        System.out.println(LeftMembers);

        return LeftMembers;
    }
}


