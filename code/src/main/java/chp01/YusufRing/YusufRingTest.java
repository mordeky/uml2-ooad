package chp01.YusufRing;// package chp01.YusufRing;
//
// import org.junit.After;
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.junit.runners.Parameterized;
//
// import java.util.Arrays;
// import java.util.Collection;
//
// import static junit.framework.TestCase.assertEquals;
//
// /**
//  * YusufRing Tester.
//  *
//  * @author <Authors name>
//  * @version 1.0
//  * @since <pre>二月 26, 2020</pre>
//  */
//
// @RunWith(Parameterized.class)
// public class YusufRingTest {
//     private int[] param;
//     private String result;
//
//     //构造函数，对变量进行初始化
//     public YusufRingTest(int[] param, String result) {
//         this.param = param;
//         this.result = result;
//     }
//
//
//     @Before
//     public void before() throws Exception {
//     }
//
//     @After
//     public void after() throws Exception {
//     }
//
//     @Parameterized.Parameters
//     public static Collection data() {
//         return Arrays.asList(new Object[][]{
//                 {
//                         new int[]{100, 3}, "58, 91"
//                 },
//                 {
//                         new int[]{100, 4}, "34, 45, 97"
//                 },
//                 {
//                         new int[]{100, 5}, "26, 47, 51, 79"
//                 },
//                 // {
//                 //         1, "ERROR"
//                 // },
//         });
//     }
//
//     /**
//      * Method: getLeftMembers(int max_val, int M)
//      */
//     @Test
//     public void testGetLeftMembers() throws Exception {
//         assertEquals(result,
//                 YusufRing.getLeftMembers(param[0], param[1]));
//     }
//
// }
