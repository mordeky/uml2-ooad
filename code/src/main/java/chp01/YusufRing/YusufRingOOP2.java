package chp01.YusufRing;

/**
 * 题目要求：有n个人围成一圈，顺序排号。从第一个人开始报数（从1到M报数），
 * 凡报到 M 的人退出圈子，问最后留下的是原来第几号的那位。
 */


class Counter2 {
    private int value;
    public int n;
    private int M;
    public int left;

    public int out() {
        if (++value == M) {
            this.left--;
            value = 0;
            return 0;
        }
        return value;
    }

    public boolean end(){
        return left < M;
    }

    Counter2(int n, int M) {
        value = 0;
        this.n = n;
        this.M = M;
        this.left = n;
    }
}

class Filter {
    public int value;
    public boolean quit = false;
    public Filter next;

    public int out(Counter2 counter) {
        while (true) {
            if (quit)
                return value;
            if (counter.out() == 0)
                quit = true;
            return value;
        }
    }

    Filter(int f) {
        value = f;
        next = null;
    }
}

class Sieve {
    private boolean finished = false;
    private Counter2 counter;
    private Filter head, cur;

    public void action() {
        while (!counter.end()) {
            int n = cur.out(counter);
            if (n == counter.n) { //|| cur.next == null
                finished = true;
                cur = head;
                continue;
            }
            if (!finished)
                cur.next = new Filter(n + 1);

            cur = cur.next;
        }
    }

    public String out(){
        this.action();

        String x = "";
        cur = head;
        while(cur != null){
            if(!cur.quit)
                x += cur.value + " ";
            cur = cur.next;
        }
        return x;
    }

    Sieve(Counter2 counter) {
        this.head = new Filter(1);
        this.cur = this.head;
        this.counter = counter;
        System.out.println("Sieve() is called");
    }
}

public class YusufRingOOP2 {

    public static void main(String args[]) {
        // int n = 10, M = 3;
        int n = 100, M = 5;
        Counter2 c = new Counter2(n, M);
        Sieve s = new Sieve(c);
        System.out.println(s.out());
    }
}
