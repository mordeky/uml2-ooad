package chp07.movie_rental;

import chp07.movie_rental.Movie;

public class Rental {
    private int daysRented;

    Rental(int daysRented){
        this.daysRented = daysRented;
    }

    public int getDaysRented() {
        return this.daysRented;
    }

    public Movie getMovie() {
        return new Movie(15);
    }

}

