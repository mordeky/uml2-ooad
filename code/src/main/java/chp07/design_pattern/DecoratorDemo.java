package chp07.design_pattern;

abstract class Sandwich {
    //...other properties and methods
    public abstract float getPrice();

}

class BasicHamburger extends Sandwich {
    public float getPrice() {
        return 10.0f;
    }
}

abstract class SandwichDecorator extends Sandwich {
    protected Sandwich sandwich;

    SandwichDecorator(Sandwich sandwich) {
        this.sandwich = sandwich;
    }

    public Sandwich getSandwich() {
        return sandwich;
    }

    public float getPrice() {
        return sandwich.getPrice();
    }

}

class CheeseDecorator extends SandwichDecorator {
    CheeseDecorator(Sandwich sandwich) {
        super(sandwich);
    }

    @Override
    public float getPrice() {
        return sandwich.getPrice() + 10.f;
    }

}


class OnionDecorator extends SandwichDecorator {
    OnionDecorator(Sandwich sandwich) {
        super(sandwich);
    }

    @Override
    public float getPrice() {
        return sandwich.getPrice() + 5.0f;
    }
}

class TomatoDecorator extends SandwichDecorator {
    TomatoDecorator(Sandwich sandwich) {
        super(sandwich);
    }

    @Override
    public float getPrice() {
        return sandwich.getPrice() + 2.0f;
    }
}

public class DecoratorDemo {
    public static void main(String[] args) throws Exception {
        Sandwich basicHamburger = new BasicHamburger();
        System.out.println("The price of basic hamburger: " + basicHamburger.getPrice());

        Sandwich cheeseHamburger = new CheeseDecorator(basicHamburger);
        System.out.println("The price of (hamburger + cheese): " + cheeseHamburger.getPrice());

        Sandwich onionHamburger = new OnionDecorator(basicHamburger);
        System.out.println("The price of (hamburger + onion): " + onionHamburger.getPrice());

        Sandwich tomatoHamburger = new TomatoDecorator(basicHamburger);
        System.out.println("The price of (hamburger + tomato): " + tomatoHamburger.getPrice());

        Sandwich tomatoOninoHamburger = new TomatoDecorator(onionHamburger);
        System.out.println("The price of (hamburger + tomato + onion): " + tomatoOninoHamburger.getPrice());

        Sandwich cheeseTomatoOninoHamburger =  new CheeseDecorator(new TomatoDecorator(onionHamburger));
        System.out.println("The price of (hamburger + tomato + onion + cheese): " + cheeseTomatoOninoHamburger.getPrice());

    }
}
