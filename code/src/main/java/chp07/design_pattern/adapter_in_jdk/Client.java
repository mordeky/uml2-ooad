package chp07.design_pattern.adapter_in_jdk;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client {
    @Test
    public void test_io() {
        /**
         * 本例中，既可以把 InputStreamReader 简单地理解为转换器：把字节流转为字符流；
         * 也可以将其理解为“适配器”：为了使用 target类（也就是字符流类“Reader"）的readLine()方法，
         * 需要将 adaptee（字节流类”FileInputStream"）转换一下才能使用。
         *
         * 注意，理解“适配器模式”，需要摒弃”只是为了将某个方法重命名“的简单动机，
         * 只是这样处理并没有多大意义，因为只是这样做，并不能。
         */
        try {
            /**
             * BufferedReader: Reads text from a character-input stream,
             * buffering characters so as to provide for
             * the efficient reading of characters, arrays, and lines.
             */

            /**
             * FileInputStream obtains input bytes from a file in a file system.
             */
            String workingPath = System.getProperty("user.dir");
            // String workingPath = new File( "." ).getCanonicalPath();
            FileInputStream fis = new FileInputStream(new File(workingPath, "src/main/java/chp07/design_pattern/text.txt"));

            /**
             * InputStreamReader is a bridge from byte streams to character streams:
             *  It reads bytes and decodes them into characters using a specified charset.
             *
             * BufferedReader: Reads text from a character-input stream,
             *  buffering characters so as to provide for
             *  the efficient reading of characters, arrays, and lines.
             */
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            // XmlAdapter chp07.design_pattern.adapter = new XmlAdapter();

            String line = reader.readLine();
            System.out.println(line);

            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_asList() {
        String[] asset = {
                "equity", "stocks", "gold", "foreign exchange",
                "fixed income", "futures", "options"};
        List<String> assetList = Arrays.asList(asset); // Arrays扮演Adapter的角色
        ArrayList assetList2 = new ArrayList();
        for(int i=0; i < asset.length; i++)
            assetList2.add(asset[i]);

        // ArrayList assetList3 = new ArrayList();
    }
}

