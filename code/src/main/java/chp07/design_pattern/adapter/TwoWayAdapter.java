package chp07.design_pattern.adapter;

/**
 * 双向适配器
 * 向适配器也可以实现双向的适配，前面所讲的都是把 Adaptee 适配成为 Target，
 * 其实也可以把 Target 适配成为 Adaptee，
 * 也就是说这个适配器可以同时当作 Target 和 Adaptee 来使用。
 *
 * 双向适配器同时实现了 Target 和 Adaptee 的接口，
 * 使得双向适配器可以在 Target 或 Adaptee 被使用的地方使用，以提供对所有客户的透明性。
 * 尤其在两个不同的客户需要用不同的地方查看同一个对象时，适合使用双向适配器。
 *
 * Ref:
 *  https://blog.csdn.net/u013679744/article/details/77679522
 *  https://blog.csdn.net/Fouse_/article/details/76681771
 */
public class TwoWayAdapter implements ITarget, IAdaptee {
	private IAdaptee adaptee;
	private ITarget target;

	/**
	 * 构造方法，传入需要被适配的对象
	 * @param adaptee 需要被适配的对象
	 */
	public TwoWayAdapter(IAdaptee adaptee){
		this.adaptee = adaptee;
	}

	public TwoWayAdapter(ITarget target){
		this.target = target;
	}

	@Override
	public void request() {
		// TODO Auto-generated method stub
		adaptee.specificRequest();
	}

	@Override
	public void specificRequest() {
		target.request();
	}
}

