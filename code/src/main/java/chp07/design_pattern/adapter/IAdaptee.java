package chp07.design_pattern.adapter;

/*
 * 已经存在的接口，这个接口需要配置
 */
public interface IAdaptee {
	/*
	 * 原本存在的方法
	 */
	public void specificRequest();
}
