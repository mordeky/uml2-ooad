package chp07.design_pattern.adapter;

/*
 * 定义客户端使用的接口，与业务相关
 */
public class Target implements ITarget {
	/*
	 * 客户端请求处理的方法
	 */
	public void request() {
		System.out.println("Target.request() is called!");
	}
}
