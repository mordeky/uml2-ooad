package chp07.design_pattern.adapter;


/**
 * 使用适配器的客户端
 * 在本质上，适配器就是一种转换器
 * 例如：笔记本电脑的就电源线上的变压器就是个适配器，不管它外部有多少伏电压过来，只要经过了适配器就能转变成我电脑需要的伏数，其中适配器起到了桥梁作用
 * 在本例中，通过Adapter把 adaptee 转换成了 target
 */
public class Client {
	public static void main(String[] args){
		//创建需要被适配的对象
		Adaptee adaptee = new Adaptee();
		/**
		 * 创建客户端需要调用的接口对象
		 * 注意：这一步操作，相当于类型转换：把 Adaptee 类转换成了 Target 类
		 *
		 * 其更重要的意义在于，当需要改变“被适配者（Adaptee）”的行为时，只需要将其替换一个新的Adaptee就可以了；
		 * 所以，从这个意义上说（LSP准则：可替换性），Adapter中的Adaptee参数最好定位为抽象类或接口。
		 */
		ITarget target = new Adapter(adaptee);
		//请求处理
		target.request();
	}
}

