package chp07.design_pattern.adapter;

/*
 * 定义客户端使用的接口，与业务相关
 */
public interface ITarget {
	/*
	 * 客户端请求处理的方法
	 */
	public void request();
}
