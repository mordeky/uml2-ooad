package chp07.design_pattern.adapter;


/*
 * 使用适配器的客户端
 */
public class ClientForTwoWayAdapter {
	public static void main(String[] args){
		//创建需要被适配的对象
		IAdaptee adaptee = new Adaptee();
		//创建客户端需要调用的接口对象
		ITarget target = new TwoWayAdapter(adaptee);
		//请求处理
		target.request();

		//创建需要被适配的对象
		target = new Target();
		//创建客户端需要调用的接口对象
		adaptee = new TwoWayAdapter(target);
		//请求处理
		adaptee.specificRequest();
	}
}

