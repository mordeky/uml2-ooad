package chp07.design_pattern.adapter;

/*
 * 适配器类
 */
public class Adapter implements ITarget {
	/*
	 * 持有需要被适配的接口对象
	 */
	private IAdaptee adaptee;
	/*
	 * 构造方法，传入需要被适配的对象
	 * @param adaptee 需要被适配的对象
	 */
	public Adapter(IAdaptee adaptee){
		this.adaptee = adaptee;
	}
	@Override
	public void request() {
		// TODO Auto-generated method stub
		adaptee.specificRequest();
	}
}

